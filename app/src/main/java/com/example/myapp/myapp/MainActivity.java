package com.example.myapp.myapp;

import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

public class MainActivity extends AppCompatActivity {
    private final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final ImageView imageView = (ImageView) findViewById(R.id.img);
        final ImageView imageView0 = (ImageView) findViewById(R.id.img0);
        final ImageView imageView1 = (ImageView) findViewById(R.id.img1);
        final ImageView imageView2 = (ImageView) findViewById(R.id.img2);
        final ImageView imageView3 = (ImageView) findViewById(R.id.img3);
        final ImageView imageView4 = (ImageView) findViewById(R.id.img4);
        final ImageView imageView5 = (ImageView) findViewById(R.id.img5);
        Log.e(TAG, "虚拟机最大运行内存: " + Runtime.getRuntime().maxMemory() / 1024 / 1024f + "MB");
        Glide.with(this).load("http://ad.zhengbajing.net/Uploads/placard/images/5992619d57bcf89vfna.jpg").into(new SimpleTarget<Drawable>() {
            @Override
            public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                imageView.setImageDrawable(resource);
            }
        });

        /**
         *
         * WebP转换工具下载 http://isparta.github.io/index.html
         */

        Glide.with(this).load("http://ad.zhengbajing.net/Uploads/placard/images/5992aec957bcf3jknww.jpg").into(imageView0);
        Glide.with(this).load("file:///android_asset/5992aea7632eavda7b3.webp").into(imageView1);
        Glide.with(this).load("file:///android_asset/5992aec957bcf3jknww.webp").into(imageView2);
        Glide.with(this).load("file:///android_asset/5992aedea4083w7mfux.webp").into(imageView3);
        Glide.with(this).load("file:///android_asset/5992aef19c671hilc16.webp").into(imageView4);
        Glide.with(this).load("file:///android_asset/5992619d57bcf89vfna.webp").into(imageView5);

    }


}
